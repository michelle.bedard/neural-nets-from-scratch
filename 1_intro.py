# %%
# imports
import numpy as np

# %%
# simple example of neuron's output value

input = np.array([1, 2, 3])
weights = np.array([0.03, 1.4, 0.08])
bias = [0.5]

output = (input * weights) + bias
print(output)
# %%
print("element 1")
print(1 * 0.3)
print((1 * 0.3) + 0.5)

print("element 2")
print(2 * 1.4)
print((2 * 1.4) + 0.5)

print("element 3")
print(3 * 0.08)
print((3 * 0.08) + 0.5)

# %%
